package com.example.bankappdigicare.enums;

public enum TransactionType {
    Deposit, Withdrawal
}
