package com.example.bankappdigicare.controllers;


import com.example.bankappdigicare.Dto.AccountCreationDto;
import com.example.bankappdigicare.Dto.Depositdto;
import com.example.bankappdigicare.Dto.Withdrawaldto;
import com.example.bankappdigicare.Services.AccountService;
import com.example.bankappdigicare.models.Account;
import com.example.bankappdigicare.payload.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/account/")
public class AccountController {

    @Autowired
    AccountService accountService;

    HttpHeaders headers = new HttpHeaders();
    String message = "success";

    @PostMapping("/createAccount")
    @ResponseBody
    public ResponseEntity<?> registerUsAer(@RequestBody AccountCreationDto accountCreationDto) throws Exception {
        ApiResponse response = new ApiResponse();

        if(response!=null){
            accountService.createAccount(accountCreationDto.getAccountName(), accountCreationDto.getAccountPassword(), accountCreationDto.getInitialDeposit());
            headers.add("message",message);
            return new ResponseEntity<>(response,headers, HttpStatus.OK);
        }
        headers.add("message","Failed");
        return new ResponseEntity<>("Account already exists",headers,HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/viewAccount/{accountNumber}")
    public ResponseEntity<?> viewAccount(@PathVariable(value = "accountNumber") String accountNumber) throws Exception {
        Account account = accountService.viewAccount(accountNumber);
        if(account!=null){
            headers.add("message",message);
            return new ResponseEntity<>(account,headers,HttpStatus.OK);
        }

        headers.add("message","Failed");
        return new ResponseEntity<>("Account not available",headers,HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawal(@RequestBody Withdrawaldto withdrawaldto) throws Exception {
        Account account = accountService.withdrawal(withdrawaldto.getAccountNumber(), withdrawaldto.getWithdrawnAmount(),withdrawaldto.getAccountPassword(), withdrawaldto.getNarration());

        if(account!=null){
            headers.add("message",message);
            return new ResponseEntity<>(account,headers,HttpStatus.OK);

        }
        headers.add("message","Failed");
        return new ResponseEntity<>("Insufficient balance",headers,HttpStatus.BAD_REQUEST);

    }


    @GetMapping("/viewTransactions/{accountNumber}")
    public ResponseEntity<?> viewTransactios(@PathVariable(value = "accountNumber") String accountNumber) throws Exception {
        return ResponseEntity.ok(accountService.viewAccount(accountNumber));
    }

    @ResponseBody
    @PostMapping("/deposit")
    public ResponseEntity<?> deposit(@RequestBody Depositdto depositdto) throws Exception {
        Account account = accountService.deposit(depositdto.getAccountNumber(), depositdto.getAmount(), depositdto.getNarration());
        String message= "success";
        if(account!=null){
            headers.add("message",message);
            return new ResponseEntity<>(account,headers,HttpStatus.OK);
        }
        headers.add("message","Failed");
        return new ResponseEntity<>("Invalid amount",headers,HttpStatus.BAD_REQUEST);
    }
}

