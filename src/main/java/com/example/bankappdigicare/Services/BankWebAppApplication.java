package com.example.bankappdigicare.Services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankWebAppApplication.class, args);
    }

}
