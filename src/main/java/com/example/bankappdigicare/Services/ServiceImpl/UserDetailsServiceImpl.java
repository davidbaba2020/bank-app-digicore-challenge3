package com.example.bankappdigicare.Services.ServiceImpl;

import com.example.bankappdigicare.Services.AccountService;
import com.example.bankappdigicare.models.Account;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.security.auth.login.AccountNotFoundException;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountService service;


    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String accountNumber) throws UsernameNotFoundException {

        User.UserBuilder userBuilder;
        Account account = service.viewAccount(accountNumber);


            if (service.viewAccount(accountNumber).getAccountName().equalsIgnoreCase(accountNumber)){
                userBuilder =  User.withUsername(accountNumber);
                userBuilder.password(account.getAccountPassword());
            }else {
                throw new AccountNotFoundException("account not found");
            }


        return userBuilder.build();
    }
}
