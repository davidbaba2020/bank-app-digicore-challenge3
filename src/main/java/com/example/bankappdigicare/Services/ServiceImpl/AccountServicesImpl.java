package com.example.bankappdigicare.Services.ServiceImpl;

import com.example.bankappdigicare.enums.TransactionType;
import com.example.bankappdigicare.models.Transaction;
import com.example.bankappdigicare.models.Account;
import com.example.bankappdigicare.payload.ApiResponse;
import com.example.bankappdigicare.Services.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class AccountServicesImpl implements AccountService {

    Map<String, Account> bankAccounts = new HashMap<>();
    List<Transaction> transactionsCabinet = new ArrayList<>();
    Account account;


    @Override
    public List<Transaction> getAccountStatement(String accountNumber) {
        List<Transaction> transactions = new ArrayList<>();
        for(Transaction t: transactionsCabinet) {
          if(t.getAccountNumber().equals(accountNumber)) {
              transactions.add(t);
          }
        }
        return transactions;
    }


    @Override
    public Account getAccountInformation(String accountNumber, String password) {
        return null;
    }
    

    @Override
    public Account deposit(String accountNumber, Double amount, String narration) {
        account = bankAccounts.get(accountNumber);
        Transaction transaction = new Transaction();
        Double existingBalance = account.getBalance();


        if(amount > 1000000 || amount < 1) {
            return null;
        }
        Double newBalance = existingBalance + amount;
        account.setBalance(newBalance);
        bankAccounts.replace(accountNumber, account);

        transaction.setAccountNumber(accountNumber);
        transaction.setTransactionType(TransactionType.Deposit);
        transaction.setTransactionDate(new Date());
        transaction.setAccountBalanceAfterTransaction(newBalance);
        transaction.setAmount(amount);
        transaction.setNarration("Your account has been credited with the sum of "+newBalance);
        transactionsCabinet.add(transaction);


        return account;
    }

    @Override
    public Account withdrawal(String accountNumber, Double withdrawalAmount, String accountPassword, String narration)  {
        Account account = bankAccounts.get(accountNumber);
        Transaction transaction = new Transaction();
        Double existingBalance = account.getBalance();
        if (withdrawalAmount < existingBalance) {
            Double newBalance = existingBalance - withdrawalAmount;
            account.setBalance(newBalance);
            bankAccounts.replace(accountNumber, account);
            transaction.setAmount(withdrawalAmount);
            transaction.setTransactionType(TransactionType.Withdrawal);
            transaction.setTransactionDate(new Date());
            transaction.setAccountBalanceAfterTransaction(newBalance);
            transaction.setAccountNumber(accountNumber);
            transaction.setNarration("your account has been debited by "+withdrawalAmount);
            transactionsCabinet.add(transaction);

            return account;
        } else {
            return null;
        }
    }

    @Override
    public ApiResponse createAccount(String accountName, String accountPassword, Double initialDeposit) {

        Random r = new Random(System.currentTimeMillis());
        ApiResponse response;
        Account account = new Account();
        Transaction transaction = new Transaction();
        for (Map.Entry<String, Account> map : bankAccounts.entrySet()) {
            if (map.getValue().getAccountName().equals(accountName)) {
                return null;
            }
        }
//
         account =  Account.builder()
                    .accountName(accountName)
                    .initialDeposit(initialDeposit)
                    .balance(account.getBalance())
                    .accountPassword(accountPassword)
                    .accountNumber(10000 + r.nextInt(20000)+""+(100 + r.nextInt(200))+""+(10 + r.nextInt(20)))
                    .build();


        transaction = Transaction.builder()
                    .transactionDate(new Date())
                    .transactionType(TransactionType.Deposit)
                    .accountBalanceAfterTransaction(initialDeposit)
                    .narration("Account has been created")
                    .amount(initialDeposit)
                    .accountNumber(account.getAccountNumber())
                    .build();
//        transaction.setTransactionDate(new Date());
//        transaction.setTransactionType(TransactionType.Deposit);
//        transaction.setAccountBalanceAfterTransaction(initialDeposit);
//        transaction.setNarration("Account has been created");
//        transaction.setAmount(initialDeposit);
//        transaction.setAccountNumber(account.getAccountNumber());

        bankAccounts.put(account.getAccountNumber(), account);
        transactionsCabinet.add(transaction);
        response = new ApiResponse(account, Boolean.TRUE, "Account successfully created", HttpStatus.OK);
        return response;
    }

    @Override
    public void login(String accountNumber, String accountPassword) {

    }

    @Override
    public Account viewAccount(String accountNumber) {
        return null;
    }

}
