package com.example.bankappdigicare.Services;


import com.example.bankappdigicare.models.Account;
import com.example.bankappdigicare.models.Transaction;
import com.example.bankappdigicare.payload.ApiResponse;
import java.util.List;

public interface AccountService {
    Account getAccountInformation (String accountNumber, String password);
    List<Transaction> getAccountStatement(String accountNumber);
    Account deposit(String accountNumber, Double amount, String narration);
    Account withdrawal(String accountNumber, Double withdrawalAmount, String accountPassword, String narration) throws Exception;
    ApiResponse createAccount(String accountName, String accountPassword, Double initialDeposit) throws Exception;
    void login(String accountNumber, String accountPassword);
    Account viewAccount(String accountNumber) throws Exception;
}
