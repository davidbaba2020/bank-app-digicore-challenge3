package com.example.bankappdigicare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages={"com.example.bankappdigicare","com.example.bankappdigicare.configuration"})
public class BankAppDigicareApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAppDigicareApplication.class, args);
    }

}
