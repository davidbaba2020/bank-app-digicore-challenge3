package com.example.bankappdigicare.models;

import com.example.bankappdigicare.enums.TransactionType;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Transaction {
    Date transactionDate;
    TransactionType transactionType;
    String narration;
    Double amount;
    Double accountBalanceAfterTransaction;
    String accountNumber;
}
