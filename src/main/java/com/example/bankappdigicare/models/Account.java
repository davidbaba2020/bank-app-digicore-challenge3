package com.example.bankappdigicare.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Account {
    String accountName;
    Double initialDeposit;
    Double balance;
    String accountNumber ;
    String accountPassword;

}