package com.example.bankappdigicare.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter@Getter
public class AccountRegistrationDto {
    String accountName;
    String accountPassword;
    Double initialDeposit;
    String depositNarration;

}
