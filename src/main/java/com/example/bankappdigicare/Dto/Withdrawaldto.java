package com.example.bankappdigicare.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Withdrawaldto {
    String accountNumber;
    Double withdrawnAmount;
    String accountPassword;
    String narration;

}
