package com.example.bankappdigicare.Dto;

import jdk.jfr.DataAmount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreationDto {
    private String accountName;
    private String accountPassword;
    private Double initialDeposit;
}
